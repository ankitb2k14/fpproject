angular.module('customShirts')
	.factory('CommonServices', commonServices);

	commonServices.$inject = [
		'$http',
		'Upload',
		'$q'
	];

	function commonServices(
		$http,
		Upload,
		$q
		){
		return {
			uploadImage: uploadImage,
			getImage: getImage,
			deleteImage: deleteImage
		}

		function uploadImage(data){
			var request = Upload.upload({
		       url: '/upload',
		       data: data
			 });
			return(request
	                .then(uploadImageSuccess)
	                .catch(uploadImageError));
	
	        /*getElasticSearchSuggest error function*/
	        function uploadImageError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function uploadImageSuccess(response) {
	            return(response);
	        }
		};

		function getImage(){
			var request = $http({
		            method: "GET",
		            url: '/image'
		        });
			return(request
	                .then(getImageSuccess)
	                .catch(getImageError));
	
	        /*getElasticSearchSuggest error function*/
	        function getImageError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function getImageSuccess(response) {
	            return(response);
	        }
		}

		function deleteImage(data){
			var request = $http({
		            method: "DELETE",
		            url: '/image/' + data.file._id
		        });
			return(request
	                .then(getImageSuccess)
	                .catch(getImageError));
	
	        /*getElasticSearchSuggest error function*/
	        function getImageError(response) {
	            if (!angular.isObject(response.data) || !response.data.message) {
	                return($q.reject(response.data));
	            }
	            /*Otherwise, use expected error message.*/
	            return($q.reject(response.data.message));
	        }
	
	        /*getElasticSearchSuggest success function*/
	        function getImageSuccess(response) {
	            return(response);
	        }
		}
	}