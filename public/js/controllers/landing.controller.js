angular.module('customShirts')
		.controller('LandingController', landingController);


		landingController.$inject = [
			'$scope',
			'$window',
			'$filter',
			'$timeout',
			'CommonServices',
			'Upload',
			'toastr'
		];

		function landingController(
			$scope,
			$window,
			$filter,
			$timeout,
			CommonServices,
			Upload,
			toastr
			){
			console.log('in landingController');
			$scope.fileUploaded = false;
			$scope.showFilters = false;

			function init(){
				CommonServices.getImage().then(function(response){
			 		if(response){
			 			if(angular.isArray(response.data)){
			 				$scope.images = response.data;
			 			}
			 		}
			 	},function(error){
			 		console.log(error);
			 	});
			}	
			init();
             $scope.downloadImage=function(src){
             	var srcNew="/uploads/"+src;
             		$('.downloadingImage').attr("download", 'download').attr("href",srcNew);
             }
			 $scope.uploadPic = function(file) {
			 	var data = { file: file};
			 	CommonServices.uploadImage(data).then(function(response){
			 		toastr.success('File Uploaded Successfully!');
			 		init();
			 	},function(error){
			 		console.log(error);
			 	});
   			 }

   			 $scope.removeImage= function(file){
   			 	if(confirm('Delete this file?')){
   			 		var data = { file: file};
	   			 	CommonServices.deleteImage(data).then(function(response){
				 		toastr.success('File Deleted Successfully!');
				 		init();
				 	},function(error){
				 		console.log(error);
				 	});
   			 	}
   			 };

			$scope.anglesliderbright = {
				value: 0,
				options: {
				loor: 0,
           		ceil: 100
				}
			};
			$scope.angleslider = {
				value: 0,
				options: {
				floor: 0,
				ceil: 360
				}
			};
            $scope.inputText = '';

             $scope.chkbxs = [{
			    label: "Grayscale",
			    val: false
			    }, {
			    label: "Sepia",
			    val: false
			    }, {
				label: "Sepia2",
				val: false
				},{
				label: "Noise",
				val: false
				},{
			    label: "Crop",
				val: false
			    },{
				label: "Invert",
				val: false
				
		    }];

			var canvas = new fabric.Canvas('canvas', {
				backgroundColor: 'rgba(100,100,200, 0.5)'
			});
				
		    $scope.$watch('anglesliderbright.value', function () {
		    	if(changeBrightness)
					changeBrightness($scope.anglesliderbright.value);
			 });
		 
    
			$scope.addText = function(inputText){
				canvas.add(new fabric.IText($scope.inputText, { 
				      left: 50,
				      top: 100,
				      fontFamily: 'arial black',
				      fill: '#333',
					    fontSize: 50
				}));
				canvas.renderAll();	

			};

            

			$scope.$watch('files', function () {
               $scope.upload($scope.files);
			});
			$scope.filters = function(){
				$scope.showFilters = !$scope.showFilters;
			};

			document.getElementById('text-color').onchange = function() {
	            canvas.getActiveObject().setFill(this.value);
	            canvas.renderAll();
	        };

	        document.getElementById('font-family').onchange = function(e) {
	            canvas.getActiveObject().setFontFamily(this.value);
	            canvas.renderAll();
        	};

        	document.getElementById('text-align').onchange = function() {
	            canvas.getActiveObject().setTextAlign(this.value);
	            canvas.renderAll();
        	};
        	document.getElementById('text-bg-color').onchange = function() {
	            canvas.getActiveObject().setBackgroundColor(this.value);
	            canvas.renderAll();
       		 };
        	document.getElementById('text-stroke-color').onchange = function() {
	            canvas.getActiveObject().setStroke(this.value);
	            canvas.renderAll();
       		 };	

       		 document.getElementById('text-stroke-width').onchange = function() {
	            canvas.getActiveObject().setStrokeWidth(this.value);
	            canvas.renderAll();
        	};

        	document.getElementById('text-font-size').onchange = function() {
	            canvas.getActiveObject().setFontSize(this.value);
	            canvas.renderAll();
       		 };

       		 radios5 = document.getElementsByName("fonttype");  // wijzig naar button
			    for(var i = 0, max = radios5.length; i < max; i++) {
			        radios5[i].onclick = function() {
			            
			            if(document.getElementById(this.id).checked == true) {
			                if(this.id == "text-cmd-bold") {
			                    canvas.getActiveObject().set("fontWeight", "bold");
			                }
			                if(this.id == "text-cmd-italic") {
			                    canvas.getActiveObject().set("fontStyle", "italic");
			                }
			                if(this.id == "text-cmd-underline") {
			                    canvas.getActiveObject().set("textDecoration", "underline");
			                }
							if(this.id == "text-cmd-linethrough") {
			                    canvas.getActiveObject().set("textDecoration", "line-through");
			                }
							if(this.id == "text-cmd-overline") {
			                    canvas.getActiveObject().set("textDecoration", "overline");
			                }
			                
			                
			                
			            } else {
			                if(this.id == "text-cmd-bold") {
			                    canvas.getActiveObject().set("fontWeight", "");
			                }
			                if(this.id == "text-cmd-italic") {
			                    canvas.getActiveObject().set("fontStyle", "");
			                }  
			                if(this.id == "text-cmd-underline") {
			                    canvas.getActiveObject().set("textDecoration", "");
			                }
							if(this.id == "text-cmd-linethrough") {
			                    canvas.getActiveObject().set("textDecoration", "");
			                }  
			                if(this.id == "text-cmd-overline") {
			                    canvas.getActiveObject().set("textDecoration", "");
			                }
			            }
			            
			            
			            canvas.renderAll();
			        }
			    }
    		$scope.upload = function (files) {
    			if(files){
    				$scope.fileUploaded = true;
    				$scope.showFilters = true;
    				$scope.file = files;
    				$scope.hgt = 930;
	    		    Upload.base64DataUrl(files).then(function(imageSrc){
	    		    	var imgObj = new Image();
				   		imgObj.setAttribute('src', imageSrc);
				   		imgObj.onload = function(){
				   			canvasImage(imageSrc)
									var changeAngle = function(customAngle){
					    				if($scope.imgInstance){
										$scope.imgInstance.set('angle', customAngle);
										
						    			canvas.renderAll();
					    				}
					    		    };
					    		    $scope.$watch('angleslider.value', function () {
				             	  		changeAngle($scope.angleslider.value);
									});
						    		var changeBrightness = function(changeBrightness){
						    			if($scope.imgInstance){
											console.log(changeBrightness)
											$scope.imgInstance.filters.push(new fabric.Image.filters.Brightness({ brightness:20 }));
											$scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
							    			canvas.renderAll();
						    			}
						    		};

					                function RemoveImage(){
									$scope.imgInstance.remove();
									}

									function canvasImage(src){
										$scope.imgInstance = new fabric.Image(imgObj , {
																		left:300,
																		top: 200,

									    });
									 //    $scope.imgInstance.setWidth(500);
									 //    $scope.imgInstance.setHeight(500);
									    
										canvas.add($scope.imgInstance);
										
										canvas.centerObject($scope.imgInstance);
										canvas.setActiveObject($scope.imgInstance);
   									    canvas.renderAll();
										
										$scope.imgInstance.set('selectable', false);
										
									}
					                
					                function rendercanvas(data){
									angular.forEach(data,function(val,key){
										console.log($scope.imgInstance)
										if(val.label=="Grayscale"){
											 $scope.imgInstance.filters.push(new fabric.Image.filters.Grayscale());
											 $scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
										}
										if(val.label=="Sepia"){
											 $scope.imgInstance.filters.push(new fabric.Image.filters.Sepia());
											 $scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
										}
										if(val.label=="Crop"){
											$scope.imgInstance.set('selectable', true);
										}
										if(val.label=="Invert"){
											$scope.imgInstance.filters.push(new fabric.Image.filters.Invert());
											$scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
								   
										}
										if(val.label=="Sepia2"){
											$scope.imgInstance.filters.push(new fabric.Image.filters.Sepia2());
											$scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
								   
										}
										if(val.label=="Noise"){
											$scope.imgInstance.filters.push(new fabric.Image.filters.Noise({
												noise: 200
											  }));
											$scope.imgInstance.applyFilters(canvas.renderAll.bind(canvas));
								   
										}
										
									})
									}

									$scope.mainDownloadWidgetCapture = function(){
										 var imageData = canvas.toDataURL("image/png");

											var file = dataURItoBlob(imageData, 'image/png');
											file.name = $scope.file.name;
										  	$scope.uploadPic(file);

									};
					            	
					            	$scope.downloadImage = function(){
										var imageData = canvas.toDataURL("image/png");
										$('#downloadImage').attr("download", 'download').attr("href", imageData);
					            	};
					            	
								    $scope.$watch("chkbxs", function(n, o) {
								    	RemoveImage();
								    	canvasImage(imageSrc)
								        $scope.trues = $filter("filter")(n, {
								            val: true
								        });
								      rendercanvas( $scope.trues)
								    }, true);

								    function dataURItoBlob(dataURI, type) {
									    // convert base64 to raw binary data held in a string
									    var byteString = atob(dataURI.split(',')[1]);

									    // separate out the mime component
									    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

									    // write the bytes of the string to an ArrayBuffer
									    var ab = new ArrayBuffer(byteString.length);
									    var ia = new Uint8Array(ab);
									    for (var i = 0; i < byteString.length; i++) {
									        ia[i] = byteString.charCodeAt(i);
									    }

									    // write the ArrayBuffer to a blob, and you're done
									    var bb = new Blob([ab], { type: type });
									    return bb;
									}
		  
				        };
	    		    });
    			}
    		}
		}