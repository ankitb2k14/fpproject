const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const ejs = require('ejs');
const db = require('./config/db'); 



//Init app
const app = express(); 

app.use(bodyParser.urlencoded({ extended: true }));
//EJS
app.set('view engine', 'ejs');

//Public Folder
app.use(express.static(__dirname + '/public'));
// app.use(express.static(__dirname + '/public/uploads'));

// app.get('/', (req, res) => res.render('index'));

const port = 3000;

MongoClient.connect(db.url, (err, database) => {
	if(err) return console.log(err);

	const dbBase = database.db('notable-1')
	require('./app/routes')(app, dbBase);
	app.listen(port, () => {
		console.log('we are live on' + port);
	});
});






