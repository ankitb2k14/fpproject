angular.module('customShirts')
	   .config(['$stateProvider', '$urlRouterProvider' , function($stateProvider, $urlRouterProvider){

	$stateProvider
		.state('landing', {
			url: '/',
			templateUrl: 'views/landing.html',
			controller: 'LandingController'
		});
		// .state('landing', {
		// 	url: '/',
		// 	templateUrl: 'views/new.html',
		// 	controller: 'NewController'
		// });
		$urlRouterProvider.when('', '/');
}]);